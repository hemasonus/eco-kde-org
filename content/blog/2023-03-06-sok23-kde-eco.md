---
date: 2023-03-06
title: "Season of KDE 2023 With KDE Eco: Setting Up Selenium For Energy Consumption Measurements"
categories:  [SOK23, Selenium, KdeEcoTest, GCompris, AT-SPI]
author: Nitin Tejuja
summary: My experience setting up Selenium using 'Selenium-AT-SPI" to measure GCompris's energy consumption.
SPDX-License-Identifier: CC-BY-SA-4.0
authors:
- SPDX-FileCopyrightText: 2023 Nitin Tejuja <@nitin.tejuja12:matrix.org>
---

I am very thankful to the KDE community for inviting me to be a part of this amazing Open Source project through their annual program [Season of KDE](https://season.kde.org).

#### About me

My name is Nitin Tejuja. I am a Software Engineer working on a platform engineering team where I develop, research, and optimize various technology projects. I have contributed to projects like Slack, GitHub Actions, and Strapi CMS, and I have created my own `nodejs` library which helps in migrating content between different Strapi CMS environments. This is my first time contributing to a project with such a large community. I'm very passinoate about solving problems and then optimizing the solutions. I enjoy learning new technologies and building excellent Open Source projects. I also believe that contributing to the KDE community is the best way to learn more about KDE projects and how we can produce environmentally-friendly Free & Open Source software.

#### What I Am Working On

In this project, we are setting up `Selenium` using Selenium AT-SPI and replicating an existing unit test written with [`KdeEcoTest`](https://invent.kde.org/teams/eco/feep/-/tree/master/tools/KdeEcoTest) to test the educational software suite [GCompris](https://apps.kde.org/gcompris/), which provides a number of activities for children aged 2 to 10. `KdeEcoTest` and `Selenium` are both useful tools for emulating user behavior in [Standard Usage Scenarios](https://eco.kde.org/handbook/#preparing-the-standard-usage-scenario-sus), represented in "Emulate Actions with Tool" in the workflow below.

{{< container class="text-center" >}}

![Steps for preparing Standard Usage Scenario (SUS) scripts to measure the energy consumption of software. (Image from the [KDE Eco Handbook](https://eco.kde.org/handbook/) published under a [CC-BY-SA-4.0](https://spdx.org/licenses/CC-BY-SA-4.0.html) license.)](/blog/images/sec3_PreparingSUS.webp)

{{< /container >}}

By measuring and then comparing the energy needed by `KdeEcoTest` and `Selenium` themselves, we can decide whether and how we should use `Selenium` for measuring the energy consumption of other software. Selenium AT-SPI for QT is still in an early stage but it is relevant to KDE as a unit testing tool, and given its flexibility it could become a great tool for energy consumption measurements.

I am writing a guide for installing [`selenium-webdriver-at-spi`](https://invent.kde.org/sdk/selenium-webdriver-at-spi) and the GCompris application scripts using selenium-webdriver-at-spi. The aim is to provide assistance to developers to create their own KDE application tests, either as a system testing tool or a energy measurement tool.

#### Motivation Behind Working On The KDE Eco Project

I have a strong interest in ecology and want to develop techniques that will reduce the impact of new technology on the planet. At the moment, KDE Eco is using `KdeEcoTest` for the energy consumption measurements of GCompris, among other applications. In this project, we will write unit tests of GCompris using `Selenium`. By using `Selenium` we can access application elements using names and many other different features, which provides more flexibility than what `KdeEcoTest` is currently able to do. Moreover, `Selenium` works with Wayland, which is not the case with `KdeEcoTest`.

#### What I Have Done & Will Be Doing In The Coming Weeks

For the first two weeks of the project, I was working on understanding `KdeEcoTest` and exploring `selenium-webdriver-at` before writing the unit test scripts.

In the first week I set up GCompris and explored the application's activities. I then turned to the `KdeTestEcoCreator` script to understood how the code present in [FOSS Energy Efficiency Project (FEEP)](https://invent.kde.org/teams/eco/feep/-/tree/master/tools) repository is working.

In the second week I started writing unit test scripts using [`selenium-webdriver-at-spi`](https://invent.kde.org/sdk/selenium-webdriver-at-spi), for which I have written an [installation guide](https://invent.kde.org/nitintejuja/feep/-/blob/selenium/tools/KdeEcoTestSelenium/setup.txt). It is already possible to use this guide to install a Selenium accessibility server to test your own KDE application!

With everything all set up, I am now learning how to write unit scripts and have written my first Selenium `Python` script which simply opens GCompris.

{{< container class="text-center" >}}

![Opening GCompris with my first Selenium `Python` script. (Image from Nitin Tejuja published under a [CC-BY-SA-4.0](https://spdx.org/licenses/CC-BY-SA-4.0.html) license.)](/blog/images/open-gcompris-script.gif)

{{< /container >}}

In the coming weeks, I will write unit scripts in `Python` to perform full testing for several activities in GCompris, which will replicate the behavior already scripted using `KdeEcoTest`. To enable event handling on activity elements, I need to dive into GCompris's `QML` code and add `QML` accessibility code in order to locate and use the GCompris elements.

Once this is completed, both `Selenium` and `KdeEcoTest` can be used to measure the energy consumption of GCompris and it will be possible to compare the two emulation tools directly.

#### Community Bonding (SoK’23)

I am thankful to my mentors Emmanuel Charruau and Harald Sitter for taking time to help me by providing resources and solving my doubts.

I am also thankful to you for taking the time to read this update. If you would like to access the scripts, they can be found [here](https://invent.kde.org/nitintejuja/feep/-/tree/selenium/tools/KdeEcoTestSelenium/scripts).

Contact me on Matrix at @nitin.tejuja12:matrix.org.